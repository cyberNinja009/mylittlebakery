using System;

namespace Domain
{
    public class Pack
    {
        public Guid Id { get; set; }
        public int PackSize { get; set; }
        public double Price { get; set; }
        public Product Product { get; set; }
    }
}