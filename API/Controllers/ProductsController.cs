using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Application.Models;
using Application.Products;
using Application.Services.OrderProcess;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class ProductsController : BaseApiController
    {
        private readonly IOrderProcessService _orderProcessService;
        public ProductsController(IOrderProcessService orderProcessService)
        {
            _orderProcessService = orderProcessService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProducts()
        {
            return HandleResult(await Mediator.Send(new List.Query()));
        }

        [HttpGet("/api/Packs/{code}")]
        public async Task<IActionResult> GetPacksDetails(string code)
        {
            return HandleResult(await Mediator.Send(new PacksDetails.Query { Code = code }));
        }

        [HttpPost("/api/Order")]
        public async Task<IActionResult> GetBillingInfo(List<OrderRequestModel> orders)
        {
            return HandleResult(await Mediator.Send(new BillingInfo.Query { Orders = orders }));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditProduct(Guid id, Product product)
        {
            product.Id = id;
            return HandleResult(await Mediator.Send(new Edit.Command { Product = product }));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            return HandleResult(await Mediator.Send(new Delete.Command { Id = id }));
        }
    }
}