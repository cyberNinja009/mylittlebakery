# 🍞 My Little Bakery 🍩

## Description

My Little🍮 is a web api helping the shop owner determine the cost and pack breakdown for each product when the customer place an order.

## Terminology/Pattern

1. Clean Architecture
2. CQRS + MediatoR

## Thoughts💡

I utilised CQRS + MediatoR pattern and aimed at building a clean, scalable application. Of course, There is no perfect pattern that solves every possible coding problem while different applications lend themselves to different patterns more than they do other patterns.

## Run▶️

Prerequisites: .Net SDK (>=5.0.101)

In root of the app run

> \$ dotnet restore

and

> \$ dotnet build

Then run

> \$ dotnet run watch -p API

The web api will run on http://localhost:5000

## Usage

### 1. Open your browser and go to http://localhost:5000/swagger . It offers a web-based UI that provides information about the service.

![Swagger UI](/Assets/Captures/swagger.png)

### 2. (Preferred) Install [Postman](https://www.postman.com/) and import the [Bakery.postman_collection.json](/Assets/Postman/Bakery.postman_collection.json) file to your Postman Client.

I have predefined both happy paths and unhappy paths for you to play with the API;

![Postman Demo](/Assets/Captures/postman.gif)

## Test

> \$ dotnet test

## License

It is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
