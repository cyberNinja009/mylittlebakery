using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;

namespace Persistence
{
    public class Seed
    {
        public static async Task SeedData(DataContext context)
        {
            if (context.Products.Any()) return;

            var products = new List<Product>
            {
                new Product
                {
                    Name = "Vegemite Scroll ",
                    CreateDate = DateTime.Now.AddMonths(-3),
                    Code = "VS5",
                    Packs = new List<Pack>()
                    {
                        new Pack()
                        {
                            PackSize = 3,
                            Price = 6.99
                        },
                        new Pack()
                        {
                            PackSize = 5,
                            Price = 8.99
                        },
                    }
                },
                new Product
                {
                    Name = "Blueberry Muffin",
                    CreateDate = DateTime.Now.AddMonths(-2),
                    Code = "MB11",
                    Packs = new List<Pack>()
                    {
                        new Pack()
                        {
                            PackSize = 2,
                            Price = 9.95,
                        },
                        new Pack()
                        {
                            PackSize = 5,
                            Price = 16.95
                        },
                        new Pack()
                        {
                            PackSize = 8,
                            Price = 24.95
                        },
                    }
                },
                new Product
                {
                    Name = "Croissant",
                    CreateDate = DateTime.Now.AddMonths(-1),
                    Code = "CF",
                    Packs = new List<Pack>()
                    {
                        new Pack()
                        {
                            PackSize = 3,
                            Price = 5.95
                        },
                        new Pack()
                        {
                            PackSize = 5,
                            Price = 9.95
                        },
                        new Pack()
                        {
                            PackSize = 9,
                            Price = 16.99
                        },
                    }
                }
            };

            await context.Products.AddRangeAsync(products);
            await context.SaveChangesAsync();
        }
    }
}