using Application.Models;
using Application.Services.OrderProcess;
using Domain;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BakeryTest

{
	public class OrderProcessServiceTest
	{
		private static DbContextOptions<DataContext> dbContextOptions = new DbContextOptionsBuilder<DataContext>().UseInMemoryDatabase(databaseName: "bakeryTest").Options;

		DataContext _context;
		OrderProcessService _orderProcessService;

		[OneTimeSetUp]
		public void Setup()
		{
			_context = new DataContext(dbContextOptions);
			_context.Database.EnsureCreated();

			SeedDatabase();

			_orderProcessService = new OrderProcessService();

		}

		[OneTimeTearDown]
		public void CleanUp()
		{
			_context.Database.EnsureDeleted();
		}

		[Test]
		public async Task GetIndividualBilling_WithValidOrderQuantity_WithPacksOptions()
		{
			var order = new OrderRequestModel()
			{
				Code = "VS5",
				Quantity = 10
			};

			var products = await _context.Products.ToListAsync();
			var packs = products?.Where(p => p.Code == order.Code)?.FirstOrDefault()?.Packs?.ToList();
			var result = _orderProcessService.GetInvidualBilling(order, packs);
			Assert.That(result.Sum, Is.EqualTo("$17.98"));
			Assert.That(result.Code, Is.EqualTo("VS5"));
		}

		[Test]
		public async Task GetIndividualBilling_WithInValidOrderQuantity_WithPacksOptions()
		{
			var order = new OrderRequestModel()
			{
				Code = "VS5",
				Quantity = 2
			};
			var products = await _context.Products.ToListAsync();
			var packs = products?.Where(p => p.Code == order.Code)?.FirstOrDefault()?.Packs?.ToList();
			var ex = Assert.Throws<Exception>(() => _orderProcessService.GetInvidualBilling(order, packs));

			Assert.AreEqual("Fail to pack 2 VS5. Please review the packs info on our website.", ex.Message);
		}

		[Test]
		public async Task GetIndividualBilling_WithInvalidProductCode_WithPacksOptions()
		{
			var order = new OrderRequestModel()
			{
				Code = "VS6",
				Quantity = 10
			};

			var products = await _context.Products.ToListAsync();
			var packs = products?.Where(p => p.Code == order.Code)?.FirstOrDefault()?.Packs?.ToList();
			var ex = Assert.Throws<Exception>(() => _orderProcessService.GetInvidualBilling(order, packs));

			Assert.AreEqual("No packs options for product VS6 at the moment.", ex.Message);
		}

		[Test]
		public void GetIndividualBilling_WithValidOrderQuantity_WithEmptyPacksOptions()
		{
			var order = new OrderRequestModel()
			{
				Code = "VS5",
				Quantity = 10
			};

			var packs = new List<Pack>();
			var ex = Assert.Throws<Exception>(() => _orderProcessService.GetInvidualBilling(order, packs));

			Assert.AreEqual("No packs options for product VS5 at the moment.", ex.Message);
		}




		private void SeedDatabase()
		{
			var products = new List<Product>
			{
				new Product
				{
					Name = "Vegemite Scroll ",
					CreateDate = DateTime.Now.AddMonths(-3),
					Code = "VS5",
					Packs = new List<Pack>()
					{
						new Pack()
						{
							PackSize = 3,
							Price = 6.99
						},
						new Pack()
						{
							PackSize = 5,
							Price = 8.99
						},
					}
				},
				new Product
				{
					Name = "Blueberry Muffin",
					CreateDate = DateTime.Now.AddMonths(-2),
					Code = "MB11",
					Packs = new List<Pack>()
					{
						new Pack()
						{
							PackSize = 2,
							Price = 9.95,
						},
						new Pack()
						{
							PackSize = 5,
							Price = 16.95
						},
						new Pack()
						{
							PackSize = 8,
							Price = 24.95
						},
					}
				},

				new Product
				{
					Name = "Croissant",
					CreateDate = DateTime.Now.AddMonths(-1),
					Code = "CF",
					Packs = new List<Pack>()
					{
						new Pack()
						{
							PackSize = 3,
							Price = 5.95
						},
						new Pack()
						{
							PackSize = 5,
							Price = 9.95
						},
						new Pack()
						{
							PackSize = 9,
							Price = 16.99
						},
					}
				}
			};

			_context.Products.AddRange(products);
			_context.SaveChanges();
		}
	}
}