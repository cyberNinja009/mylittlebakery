using System.Collections.Generic;

namespace Application.Models
{
	public class BillingModel
	{
		public string Code { get; set; }
		public List<PackDetail> PackDetails { get; set; }
		public int RequestQuantity { get; set; }
		public string Sum { get; set; }

	}

	public class PackDetail
	{
		public int PackSize { get; set; }
		public int Quantity { get; set; }
		public string Price { get; set; }
	}
}