namespace Application.Models
{
	public class OrderRequestModel
	{
		public string Code { get; set; }
		public int Quantity { get; set; }
	}
}