using System.Collections.Generic;
using Application.Models;
using Domain;

namespace Application.Services.OrderProcess
{
	public interface IOrderProcessService
	{
		BillingModel GetInvidualBilling(OrderRequestModel request, List<Pack> packsOptions);
		List<List<PackDetail>> GetPossibleComboPacksForIndividualProduct(List<Pack> packsOptions, int requestQuantity, List<PackDetail> combo, int depth);
	}
}