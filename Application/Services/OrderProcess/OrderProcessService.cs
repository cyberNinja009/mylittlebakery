using System;
using System.Collections.Generic;
using System.Linq;
using Application.Constants;
using Application.Models;
using Domain;

namespace Application.Services.OrderProcess
{
	public class OrderProcessService : IOrderProcessService
	{
		public BillingModel GetInvidualBilling(OrderRequestModel request, List<Pack> packsOptions)
		{
			if (packsOptions == null || packsOptions?.Count <= 0) throw new Exception($"No packs options for product {request.Code} at the moment.");
			var possibleBreakDowns = GetPossibleComboPacksForIndividualProduct(packsOptions, request.Quantity, new List<PackDetail>(), 0);
			if (possibleBreakDowns.Count > 0)
			{
				// Sort so that the combination that uses the fewest boxes and has the highest price is first
				var result = possibleBreakDowns.OrderBy(
					combination => combination.Sum(item => item.Quantity)
				).ThenByDescending(
					combination => combination.Sum(item => item.Quantity * packsOptions.First(pack => pack.PackSize == item.PackSize).Price)
				);

				var packBreakDown = result.FirstOrDefault();
				var totalPrice = packBreakDown.Sum(item => item.Quantity * packsOptions.First(pack => pack.PackSize == item.PackSize).Price);
				var totalPriceStr = string.Format(Aliases.PRICE_FORMAT, totalPrice);
				return new BillingModel()
				{
					Code = request.Code,
					RequestQuantity = request.Quantity,
					PackDetails = packBreakDown,
					Sum = "$" + totalPriceStr
				};

			}
			else
			{
				throw new Exception($"Fail to pack {request.Quantity} {request.Code}. Please review the packs info on our website.");
			}
		}

		public List<List<PackDetail>> GetPossibleComboPacksForIndividualProduct(List<Pack> packsOptions, int requestQuantity, List<PackDetail> combo, int depth)
		{
			var packList = new List<List<PackDetail>>();

			for (int i = packsOptions.Count - 1; i >= 0; i--)
			{
				if (requestQuantity >= packsOptions[i].PackSize)
				{
					var packSize = packsOptions[i].PackSize;
					var quantity = requestQuantity / packsOptions[i].PackSize;
					combo.Add(
						new PackDetail()
						{
							PackSize = packSize,
							Quantity = quantity,
							Price = "$" + string.Format(Aliases.PRICE_FORMAT, packsOptions[i].Price)
						}
					);

					var lastEntry = combo.Last();
					var remainer = requestQuantity;

					if (lastEntry != null) remainer -= lastEntry.PackSize * lastEntry.Quantity;

					packList.AddRange(GetPossibleComboPacksForIndividualProduct(packsOptions, remainer, combo, depth + 1));
				}

				// if the totalQuantity is exactly 0 then have have made a combination that adds up to the required total
				else if (requestQuantity == 0)
				{
					if (combo.Count > 0)
					{
						// add the successful combination to the list of possible packing combinations
						packList.Add(new List<PackDetail>(combo));
						// Clear the combination so that we can try more.
						combo.Clear();
					}
				}
				//If the remaining totalQuantity is too small for any of the available pack sizes then the current combination has failed
				else if (packsOptions.All(p => p.PackSize > requestQuantity))
				{
					// if we are at the bottom of the recursion
					if (depth == packsOptions.Count - 1)
					{
						var last = combo.Last();
						// and we have tried all the possible pack sizes (tries with largest first, then smallest)
						if (last.PackSize == packsOptions[0].PackSize)
						{
							//clear the current combination so we can start again
							combo.Clear();
						}
						else
						{
							// we haven't tried all possible pack sizes yet so just remove the last one and keep trying with the remaining sizes
							combo.RemoveAt(combo.Count - 1);
						}
					}
					else
					{
						combo.Clear();
					}

					// return the current list of successful combinations.
					return packList;
				}
			}
			return packList;

		}
	}
}