using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using Application.Models;
using Application.Services.OrderProcess;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Products
{
    public class BillingInfo
    {
        public class Query : IRequest<Result<List<BillingModel>>>
        {
            public List<OrderRequestModel> Orders { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<List<BillingModel>>>
        {
            private readonly DataContext _context;
            private readonly IOrderProcessService _orderProcessService;

            public Handler(DataContext context, IOrderProcessService orderProcessService)
            {
                _context = context;
                _orderProcessService = orderProcessService;
            }
            public async Task<Result<List<BillingModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var details = new List<BillingModel>();
                foreach (OrderRequestModel order in request.Orders)
                {
                    var products = await _context.Products.Include(p => p.Packs).SingleOrDefaultAsync(x => x.Code == order.Code, cancellationToken);
                    var packs = products?.Packs?.ToList();
                    if (packs == null || packs.Count <= 0) return Result<List<BillingModel>>.Failure($"Can't retrieve the packs details for product {order.Code}");
                    details.Add(_orderProcessService.GetInvidualBilling(order, packs));
                }

                return Result<List<BillingModel>>.Success(details);
            }
        }


    }
}