using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Products
{
    public class PacksDetails
    {
        public class Query : IRequest<Result<List<Pack>>>
        {
            public string Code { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<List<Pack>>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Result<List<Pack>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var products = await _context.Products.Include(p => p.Packs).SingleOrDefaultAsync(x => x.Code == request.Code, cancellationToken);
                if (products == null) return Result<List<Pack>>.Failure($"Can't retrieve the packs details for product {request.Code}");

                return Result<List<Pack>>.Success(products.Packs.ToList());
            }
        }
    }
}